const express = require('express');

const router = express.Router();
const {
  getUserNotes,
  addUserNotes,
  getUsersNotesById,
  updateUsersNotesById,
  checkUsersNotesById,
  deleteUsersNotesById,
} = require('../services/noteServices');

router.get('/', getUserNotes);

router.post('/', addUserNotes);

router.get('/:id', getUsersNotesById);

router.put('/:id', updateUsersNotesById);

router.patch('/:id', checkUsersNotesById);

router.delete('/:id', deleteUsersNotesById);

module.exports = {
  noteRouters: router,
};
