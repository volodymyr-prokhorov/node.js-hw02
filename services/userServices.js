// User profile
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../models/userModel');

function getUserProfileInfo(req, res, next) {
  return User.findById({ _id: req.user.userId })
    .then((user) => {
      // res.json(result);
      res.status(200).json({
        user: 
        {
          _id: req.user.userId,
          username: req.user.username,
        }
      });
    });
}

function deleteUserProfile(req, res, next) {
  return User.findByIdAndDelete({ _id: req.user.userId })
    .then((result) => {
      // res.json(result);
      res.status(200).json({ message: 'Success' });
    });
}

const changeUserPassword = async (req, res, next) => {  
  const { oldPassword, newPassword } = req.body;
  const user = await User.findById({ _id: req.user.userId });

  if ( bcrypt.compare(String(newPassword), String(user.password))) {
    user.password = await bcrypt.hash(newPassword, 10);
    return user.save().then((saved) => res.json({ message: 'Success' }));
  };
}

module.exports = {
  getUserProfileInfo,
  deleteUserProfile,
  changeUserPassword,
};
