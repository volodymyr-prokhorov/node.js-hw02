const { Note } = require('../models/noteModel');

function addUserNotes(req, res, next) {
  const { text } = req.body;
  const note = new Note({
    userId: req.user.userId,
    text,
  });
  note.save().then((saved) => {
    res.json({ message: 'Success' });
  });
}

function getUserNotes(req, res, next) {
  return Note.find({ userId: req.user.userId }, '-__v').then((result) => {
    res.json({ notes: result });
  });
}

function getUsersNotesById(req, res, next) {
  return Note.findById({ _id: req.params.id, userId: req.user.userId })
    .then((note) => {
      res.json({ note: note });
    });
}

const updateUsersNotesById = (req, res, next) => {
  const { text } = req.body;
  return Note.findByIdAndUpdate(
    { _id: req.params.id, userId: req.user.userId },
    { $set: { text } },
  ).then((result) => {
    res.json({ message: 'Success' });
  });
};

const checkUsersNotesById = async (req, res, next) => {
  const note = await Note.findById({ _id: req.params.id, userId: req.user.userId });
  note.completed = !note.completed;
  return note.save().then((saved) => res.json({ message: 'Success' }));
};

function deleteUsersNotesById(req, res, next) {
  return Note.findByIdAndDelete({ _id: req.params.id, userId: req.user.userId })
    .then((note) => {
      res.json({ message: 'Success'});
    });
}

module.exports = {
  getUserNotes,
  addUserNotes,
  getUsersNotesById,
  updateUsersNotesById,
  checkUsersNotesById,
  deleteUsersNotesById,
};
