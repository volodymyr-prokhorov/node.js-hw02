// User model.
const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  username: { type: String, required: true, unique: true, },
  password: { type: String, required: true, },
  createdDate: { type: Date, default: Date.now },
});

const collectionName = 'Users';
const User = mongoose.model('User', userSchema, collectionName);

module.exports = {
  User,
};
