// Notes model.
const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  userId: { type: mongoose.Schema.Types.ObjectId, required: true },
  completed: { type: Boolean, default: false },
  text: { type: String, required: true },
  createdDate: { type: Date, default: Date.now },
});

const collectionName = 'Notes';
const Note = mongoose.model('Note', noteSchema, collectionName);

module.exports = {
  Note,
};
